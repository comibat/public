try:
    out = open ('/tmp/abcd.log','w')
    for i in range(10):
        msg = "#{}".format(i)
        out.write(msg)
    out.close()
except KeyboardInterrupt:
    print "You pressed Ctrl+C"
    sys.exit()

except socket.gaierror:
    print 'Hostname could not be resolved. Exiting'
    sys.exit()

except socket.error:
    print "Couldn't connect to server"
    sys.exit()